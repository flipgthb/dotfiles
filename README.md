# Felippe's Dotfiles

This repository contains my dotfiles and is meant to be used via the git bare repository trick as in [here](https://www.atlassian.com/git/tutorials/dotfiles), which is reproduced below, with some little changes:

## Starting from scratch

```bash
git init --bare $HOME/.cfg
alias dit='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
dit config --local status.showUntrackedFiles no
echo "alias dit='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'" >> $HOME/.bashrc
echo ".cfg" > .gitignore
dit add .gitignore
dit commit -m "Starting bare dotfiles. Added .gitignore"
```

Then setup a remote repository and push with
```bash
dit push -u origin master
```

## Cloning

use the commands

```bash
git clone --bare https://gitlab.com/flipgthb/dotfiles.git $HOME/.cfg
alias dit='/usr/bin/git --git-dir=$HOME/.cfg --work-tree=$HOME'
dit config --local status.showUntrackedFiles no
dit checkout
```

if you get errors due to conflicts, remove the files or use the following to backup the existing them

```bash
mkdir -p .config-backup && \
dit checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | \
xargs -I{} mv {} .config-backup/{}
```
