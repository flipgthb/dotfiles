function pdu
	pacman -Qei | rg -e "Name" -e "Installed Size" | xargs -d\n -n2 | awk -F" " ' {print $3,$7,$8} '
end
