#!/usr/bin/env fish

# setxkbmap br

if status --is-interactive
	# abbr --add --global myt mpv --ytdl --ytdl-format best
	abbr --add --global yt  youtube-dl
	abbr --add --global ds dit status 
	abbr --add --global da dit add 
	abbr --add --global dc dit commit -m 
	abbr --add --global dp dit push -u origin master
	abbr --add --global l exa -Fhl
	abbr --add --global la exa -Fhla
	abbr --add --global sclr 'sudo swapoff -a; sudo swapon -a'
	abbr --add --global c rsync -rvhP
	abbr --add --global p paru
	abbr --add --global zi z -I
	# abbr --add --global fi f -k
	# abbr --add --global fik f -k -w 'kak'
	# abbr --add --global gs git status 
	# abbr --add --global ga git add
	# abbr --add --global gc git commit -m
	# abbr --add --global gp git push -u origin master
	abbr --add --global b buku --suggest
end

# set -x EDITOR kak
set -x PATH ~/.emacs.d/bin ~/.local/bin ~/.local/bin/julia-*/bin ~/.cargo/bin $PATH

starship init fish | source
zoxide init fish | source
navi widget fish | source  # Ctrl + G launches navi

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
eval /home/felippe/miniforge3/bin/conda "shell.fish" "hook" $argv | source
# <<< conda initialize <<<

